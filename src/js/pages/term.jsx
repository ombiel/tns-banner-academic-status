var React = window.React = require("react");
var Page = require("-components/page");
var { VBox, Panel } = require("-components/layout");
var { BannerHeader, Header } = require("-components/header");
var { BasicSegment } = require("-components/segment");
var { Listview } = require("-components/listview");
var { Message, InfoMessage } = require("-components/message");
var _ = require("-aek/utils");

var Utils = require("../scripts/utils");

var TermPage = React.createClass({

  getInitialState: function () {
    return {
      loading: true,
      error: false,
      sortBy: "All",
      sortDesc: true
    };
  },

  componentDidMount: function () {
    this.getStatusByTerm();
    //this.forceUpdate();
  },

  getStatusByTerm: function () {
    var code = this.props.ctx.params.id;
    if (code) {
      Utils.fetchStatus(code, (err, response) => {
        if (!err && response) {
          this.setState({ response: response, loading: false });
        }
        else {
          this.setState({ error: true, loading: false });
        }
      });
    }
    else {
      this.setState({ error: true, loading: false });
    }
  },

  // Sort Function
  setSort: function (sortBy, sortDesc, ev) {
    if (ev) {
      ev.preventDefault();
    }
    this.setState({ sortBy, sortDesc });
  },

  render: function () {

    var header;
    let content = [];

    let loading = this.state.loading;
    let response = this.state.response;
    let config = this.props.config;

    if (this.state.error || !response && !loading) {

      content.push(<InfoMessage>{config.TermPage.fatal}</InfoMessage>);

    }
    else if (!loading && !this.state.error) {

      header = response ? response[0].termDescription ? <BannerHeader theme="prime" key="header" flex={0} level="2">{response[0].termDescription}</BannerHeader> : "" : "";

      let semester = !_.isEmpty(response) ? response[0] : false;

      // Default message
      content.push(<Message theme="alt" key='info-message'>Displayed below are various items which may affect your registration. Your Time Ticket, Holds, Academic Standing, Student Status, Class, and Curriculum may prevent registration or restrict the courses you will be permitted to select.</Message>);
      let checklist = [];
      let statuses = [];
      if (semester.academicStatusMessage) {
        statuses.push({ indicator: semester.academicStandingSuccess, message: semester.academicStatusMessage });
      }
      if (semester.holdMessage) {
        statuses.push({ indicator: semester.holdSuccess, message: semester.holdMessage });
      }
      if (semester.studentStatusMessage) {
        statuses.push({ indicator: semester.studentStatusSuccess, message: semester.studentStatusMessage });
      }
      if (semester.timeTicketMessage) {
        statuses.push({ indicator: semester.timeTicketSuccess, message: semester.timeTicketMessage });
      }

      if (statuses === []) {
        content.push(<InfoMessage key='error'>{config.TermPage.error}</InfoMessage>);
      }
      else {
        statuses.forEach((status) => {
          if (status.indicator === true && status.message) {
            checklist.push({ icon: 'check', text: status.message, key: status.message });
          }
          else if (status.indicator === false && status.message) {
            checklist.push({ icon: 'remove', text: status.message, key: status.message });
          }
        });
        content.push(<Listview items={checklist} className="checklist" key={"checklist"} />);
      }
    }

    return (
      <Page>
        <VBox>
          <Panel>
            {header}
            <BasicSegment loading={loading}>
              {content}
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );

  }

});

module.exports = TermPage;
