var request = require("-aek/request");
var _ = require("-aek/utils");

var AekStorage = require("-aek/storage");
var storage = new AekStorage("Tulane-Banner-Registration-Status");

class StatusUtils {

  fetchData(cb) {
    // request.get(_.publicPath("/data/test.json")).end((err, response) => {
    request.action('fetch-info').end((err, response) => {
      var body = JSON.parse(response.body.json);
      if(!_.isEmpty(body)) {
        storage.set('semesters', body);
        cb(null, body);
      } else {
        cb(true, null);
      }
    });
  }

  cachedData(cb) {
    if(storage.get('semesters') !== null) {
      var semesters = storage.get('semesters');
      // console.log(semesters);
      cb(null, semesters);
      return semesters;
    } else {
      cb(true, null);
      return null;
    }
  }

  fetchStatus(code,cb) {
    let semester = storage.get('semesters').filter(course => course.curriculumProfile.term === code);
    // console.log(semester)
    if (semester.length > 0) {
      cb(null, semester);
    } else {
      cb(true, null);
    }
  }

}

// export a single instance so all modules see the share the same thing
module.exports = new StatusUtils();
