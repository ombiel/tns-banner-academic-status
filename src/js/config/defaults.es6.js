// Default text for all static content set here
module.exports = {
  IndexPage:{
    blurb: 'Please choose a term to view registration status:',
    error: 'No semesters found',
    fatal: 'No data found'
  },
  TermPage:{
    section1:{
      header: 'Time Ticket',
      fieldname1: 'timeTicket',
      labels:{
        fromDate: '',
        fromTime: '',
        toDate: '',
        toTime: ''
      }
    },
    section2:{
      header: 'Curriculum',
      labels:{
        level: '',
        program: '',
        admitTerm: '',
        admitType: '',
        college: '',
        degree: '',
        major: '',
        concentration: ''
      }
    },
    results:{
      error: 'Registration status not available'
    },
    error: 'No grades found',
    fatal: 'No data found'
  }
};
